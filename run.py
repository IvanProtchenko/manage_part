#!env/bin/python3

from config import DBHOST, DBTABLE, DBUSER, DBPASSWORD, DBNAME, DBPARTDAY
import pymysql.cursors
import time

def partClock(t,d):
    return time.strftime('%Y-%m-%d 23:59:59',time.localtime(t+int(d)*24*60*60))

def partName(t,d):
    return 'p%s' % time.strftime('%Y_%m_%d',time.localtime(t+int(d)*24*60*60))

if __name__ == '__main__':
    # Connect to the database
    connection = pymysql.connect(host=DBHOST,
                                user=DBUSER,
                                password=DBPASSWORD,
                                db=DBNAME,
                                charset='utf8',
                                cursorclass=pymysql.cursors.DictCursor)
    try:
        with connection.cursor() as cursor:
            sql = "SELECT PARTITION_NAME FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_NAME='%s' AND TABLE_SCHEMA='%s'" % (DBTABLE,DBNAME)
            cursor.execute(sql)
            part_list = cursor.fetchall()
            if partName(time.time(),-DBPARTDAY) in [i['PARTITION_NAME'] for i in part_list if partName(time.time(),-DBPARTDAY) == i['PARTITION_NAME']]:
                sql = 'ALTER TABLE `%s` DROP PARTITION %s' % (DBTABLE,partName(time.time(),-DBPARTDAY))
                cursor.execute(sql)
                result = cursor.fetchone()
            if partName(time.time(),7) not in [i['PARTITION_NAME'] for i in part_list if partName(time.time(),7) == i['PARTITION_NAME']]:
                sql = 'ALTER TABLE `%s` ADD PARTITION (PARTITION %s VALUES LESS THAN (UNIX_TIMESTAMP("%s")))' % (DBTABLE,partName(time.time(),7),partClock(time.time(),7))
                cursor.execute(sql)
                result = cursor.fetchone()
    finally:
        connection.close()