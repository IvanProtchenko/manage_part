1. Установка
```
python3 -m venv env
env/bin/pip install -r requirements.txt
```
2. Создание файла config.py
```
#!env/bin/python3

DBHOST=''
DBUSER=''
DBPASSWORD=''
DBNAME=''
DBTABLE=''
DBPARTDAY=''
```

3. Структура таблицы zabbix 5.0.1
```
CREATE TABLE `alerts` (
  `alertid` bigint(20) unsigned NOT NULL,
  `actionid` bigint(20) unsigned NOT NULL,
  `eventid` bigint(20) unsigned NOT NULL,
  `userid` bigint(20) unsigned DEFAULT NULL,
  `clock` int(11) NOT NULL DEFAULT 0,
  `mediatypeid` bigint(20) unsigned DEFAULT NULL,
  `sendto` varchar(1024) COLLATE utf8_bin NOT NULL DEFAULT '',
  `subject` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `message` text COLLATE utf8_bin NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `retries` int(11) NOT NULL DEFAULT 0,
  `error` varchar(2048) COLLATE utf8_bin NOT NULL DEFAULT '',
  `esc_step` int(11) NOT NULL DEFAULT 0,
  `alerttype` int(11) NOT NULL DEFAULT 0,
  `p_eventid` bigint(20) unsigned DEFAULT NULL,
  `acknowledgeid` bigint(20) unsigned DEFAULT NULL,
  `parameters` text COLLATE utf8_bin NOT NULL,
  KEY `alerts_1` (`actionid`),
  KEY `alerts_2` (`clock`),
  KEY `alerts_3` (`eventid`),
  KEY `alerts_4` (`status`),
  KEY `alerts_5` (`mediatypeid`),
  KEY `alerts_6` (`userid`),
  KEY `alerts_7` (`p_eventid`),
  KEY `c_alerts_6` (`acknowledgeid`),
  KEY `alerts_0` (`alertid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
 PARTITION BY RANGE (`clock`)
```
4.
```
cat /etc/cron.d/manage_part
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

20 8 * * 5 /opt/manage_part/env/bin/python3 /opt/manage_part/run.py
```
